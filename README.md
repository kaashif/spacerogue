SpaceRogue
==========

A game where you roam the universe in a spaceship, trying to mine rocks
and kill the enemies without dying. Very simple, mostly intended as a
Python beginner project, licensed so that other beginners can take it
without fear of me suing them (BSD 2-clause).

There isn't much you can do in this version other than explore a very
bland universe, scan, mine and shoot up some objects floating in space,
so don't expect much in the way of an objective.

Install
======

Install using the setuptools script like this:

 # python3 setup.py install

Alternatively, install from PyPi (this doesn't work at the moment):

 # pip3 install spacerogue

Then run the game like this:

 $ spacerogue

It takes no options and comes with no manual page or info page other
than the in-game help, which should be plenty.

Copying
=======

Copyright (c) 2014 Kaashif Hymabaccus
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
