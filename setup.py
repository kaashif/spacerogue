#!/usr/bin/env python3
import os
from setuptools import setup

setup(
    name = "spacerogue",
    version = "0.1.0",
    author = "kaashif",
    description = ("Text roguelike in space"),
    license = "BSD",
    keywords = "game rogue space",
    url = "http://gitorious.org/kaashif-games/spacerogue",
    packages=['spacerogue'],
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Games/Entertainment",
        "License :: OSI Approved :: BSD License",
        "Programming Language :: Python :: 3.3",
        "Operating System :: POSIX"
    ],
    package_data = {
        "spacerogue":[
            "res/help/*.txt", 
            "res/json/*.json",
            "res/*.txt"]
    },
    scripts = [
        "scripts/spacerogue"
    ]
)
