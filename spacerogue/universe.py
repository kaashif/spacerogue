import random
from spacerogue.thing import Categories, Space, Asteroid, Planet, Station, Nebula, Debris
from spacerogue.player import Player
from spacerogue.gameutil import weighted_choice, clear, cat
from spacerogue.props import system_descs, system_probs

class Universe(object):
	'''Contains many star systems'''
	def __init__(self, verbose=False, size=16):
		self.verbose = verbose
		self.size = size
		self.systems = self.init_systems(size)
		self.time = 0.0

	def init_systems(self, size):
		'''Generates systems, puts into data struct'''
		return [[StarSystem((x,y),16) for y in range(size)] for x in range(size)]

	def valid_pos(self, pos):
		'''Checks if given pos is valid'''
		return 0 <= pos[0] < self.size and 0 <= pos[1] < self.size

	def get_system(self, coords):
		'''Takes a tuple of coords, returns system'''
		return self.systems[coords[0]][coords[1]]

	def advance_time(self):
		'''Adds a value to time'''
		self.time += 0.1

class StarSystem(object):
	'''Contains many sectors'''
	def __init__(self, pos, size=16):
		self.size = size
		self.category = self.choose_category()
		self.pos = pos
		self.sectors = self.init_sectors(size)

	def valid_pos(self, pos):
		'''Checks if given pos is valid'''
		return 0 <= pos[0] < self.size and 0 <= pos[1] < self.size

	def choose_category(self):
		'''Returns a 'random' category'''
		return weighted_choice([
			(Categories.Habitable, 10), (Categories.Nebula, 5),
			(Categories.MineralRich, 15), (Categories.Populated, 1),
			(Categories.Typical, 500)
		])

	def init_sectors(self, size):
		'''Generates sectors into data struct'''
		return [[Sector((self.pos,(x,y)),self.category,self) for y in range(size)] for x in range(size)]

	def scan(self, verbose=True):
		'''Returns some description of system category'''
		output = 'System: %s' % system_descs[
			int(self.category) - 1]
		return output

	def get_sector(self, coords):
		'''Takes tuple of coords, returns sector'''
		return self.sectors[coords[0]][coords[1]]

class Sector(object):
	'''Contains things'''
	def __init__(self, pos, category, system):
		self.pos = pos
		self.category = category
		self.system = system
		self.things = self.init_things()

	def clear_destroyed(self):
		'''Turns all flagged things into debris'''
		for thing in self.things:
			if thing.destroyed:
				debris = Debris(thing.position)
				debris.description = 'Debris of destroyed %s' % thing.name.lower()
				for resource,value in thing.resources.items():
					debris.resources[resource] = int(value/2)
				self.del_thing(thing)
				self.add_thing(debris)

	def init_things(self):
		'''Adds some things to sector based on system category'''
		struct = []
		choices = []
		objprobs = system_probs[self.category - 1]
		for thing, prob in objprobs.items():
			choices.append((thing, prob))
		to_add = weighted_choice(choices)
		if to_add == 'space':
			struct.append(Space(self.pos))
		elif to_add == 'asteroid':
			struct.append(Asteroid(self.pos))
		elif to_add == 'planet':
			struct.append(Planet(self.pos))
		elif to_add == 'station':
			thing = Station(self.pos)
			if thing.is_shop:
				self.system.category = Categories.Shop
			struct.append(thing)
		elif to_add == 'nebula':
			struct.append(Nebula(self.pos))
		else:
			struct.append(Space(self.pos))
		return struct

	def add_thing(self, thing):
		'''Adds thing to sector list of things'''
		self.things.append(thing)

	def del_thing(self, thing):
		'''Deletes all instances of thing in sector list'''
		try:
			self.things = [t for t in self.things if (not isinstance(t, thing))]
		except TypeError:
			self.things = [t for t in self.things if (not isinstance(t, thing.__class__))]

	def has(self, thing):
		return [t for t in self.things if isinstance(t, thing)]

	def get_things(self):
		'''Returns list of non-player things'''
		return [t for t in self.things if (not isinstance(t, Player))]

	def pick_thing(self):
		'''Chooses random non-player thing'''
		return self.get_things()[0]

	def scan(self, verbose=True):
		'''Returns scan result string'''
		result = ''
		for thing in self.things:
			if not isinstance(thing, Player):
				result += '%s: %s\n\n' % (thing.name, thing.description)
				if thing.health != 0:
					result += '	Hull integrity: %s hp\n\n' % thing.health
				result += '	Resources:\n'
				num_resources = 0
				for resource, amount in thing.resources.items():
					if amount != 0:
						num_resources += 1
						result += '	|- %s: %s tons\n' % (resource.title(),amount)
				if num_resources == 0:
					result += '	|- None\n'
				result += '\n	Items:\n'
				for item in thing.inventory:
					result += '	|- %s\n' % item.name.title()
				if len(thing.inventory) == 0:
					result += '	|- None\n'
				result += '\n'
		return result
