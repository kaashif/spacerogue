from collections import OrderedDict

class Shop(object):
	def __init__(self, screen, player):
		self.screen = screen
		self.player = player
		self.output = ''
		self.running = True
		self.to_sell = OrderedDict()
		self.to_buy = OrderedDict()

	def update_res(self):
		'''Updates resources and key to sell/buy them'''
		chars = [chr(c) for c in range(96,122)]
		num_res = 0
		for resource,amount in self.player.resources.items():
			num_res += 1
			self.to_sell[chars[num_res]] = resource
		num_res += 1
		self.to_buy[chars[num_res]] = 'ammo'

	def show(self):
		'''Shows shop screen'''
		self.output = 'Shop\n'
		for char, res in self.to_sell.items():
			cur_amt = self.player.resources[res]
			self.output += '%s: sell 10 tons of %s (%s in hold)\n' % (char,res,cur_amt)
		self.output += '\n'
		for char, res in self.to_buy.items():
			cur_amt = self.player.resources[res]
			self.output += '%s: buy 10 tons of %s (%s in hold)\n' % (char,res,cur_amt)
		self.output += '\nq: quit shop\n\n'
		self.output += self.player.show_money()
		self.screen.draw_override(self.output)
		self.screen.refresh()

	def proc_input(self):
		'''Processes input'''
		key = self.screen.get_key()
		if key == ord('q'):
			self.running = False
		try:
			inchr = chr(key)
		except:
			return None
		res = ''
		transaction = None
		try:
			transaction = self.player.sell
			res = self.to_sell[inchr]
		except LookupError:
			try:
				transaction = self.player.buy
				res = self.to_buy[inchr]
			except LookupError:
				return None
		transaction(res, 10)
