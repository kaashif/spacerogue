from spacerogue.thing import Planet, Station, Asteroid, Space, Categories, Nebula, Debris
from spacerogue.player import Player
from spacerogue.gameutil import titleize

class Map(object):
	def __init__(self, player):
		self.universe = player.universe
		self.player = player

	def show(self, scope='system'):
		'''Returns either universe/system map string'''
		if scope == 'system':
			to_map = self.universe.get_system(self.player.pos[0])
			return self.show_system(to_map)
		elif scope == 'universe':
			return self.show_universe(self.universe)

	def show_universe(self, universe):
		'''Returns universe map string'''
		format_map = titleize('Map of Universe')
		for y in range(0, universe.size):
			for x in range(0, universe.size):
				system = universe.get_system((x, y))
				if (x, y) == self.player.pos[0]:
					format_map += 'P'
				elif system.category == Categories.Typical:
					format_map += '-'
				elif system.category == Categories.Habitable:
					format_map += 'H'
				elif system.category == Categories.Nebula:
					format_map += '~'
				elif system.category == Categories.Populated:
					format_map += '#'
				elif system.category == Categories.MineralRich:
					format_map += '@'
				elif system.category == Categories.Shop:
					format_map += '$'
				else:
					format_map += '-'
			format_map += '\n'
		return format_map

	def show_system(self, system):
		'''Returns system map string'''
		format_map = titleize('Map of System')
		for y in range(0, system.size):
			for x in range(0, system.size):
				sector = system.get_sector((x, y))
				sector.clear_destroyed()
				if sector.has(Player):
					format_map += 'P'
				elif sector.has(Debris):
					format_map += '#'
				elif sector.has(Planet):
					format_map += 'O'
				elif sector.has(Station):
					format_map += 'X'
				elif sector.has(Asteroid):
					format_map += '@'
				elif sector.has(Nebula):
					format_map += '%'
				else:
					format_map += '-'
			format_map += '\n'
		return format_map
