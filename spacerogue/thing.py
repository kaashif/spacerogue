import random
from pkg_resources import resource_filename
from spacerogue.gameutil import weighted_choice, cat, time_between
from spacerogue.props import thing_descs, thing_resources, thing_properties, thing_itemprobs
from spacerogue.items import Item

class Thing(object):
	def __init__(self, pos):
		self.name = self.__class__.__name__
		self.position = pos
		self.description = self.init_desc()
		self.resources = self.init_res()
		self.properties = self.init_prop()
		self.health = self.properties['health']
		self.destroyed = False
		self.inventory = self.init_inv()

	def add_item(self,name):
		'''Adds item to inventory'''
		self.inventory.append(Item(name))

	def del_item(self,to_del):
		'''Deletes item with name from inventory'''
		names = {}
		for item in self.inventory:
			names[item.name] = item
		if not to_del in names.keys():
			return 'Item not in inventory'
		else:
			modified_inv = []
			for name, item in names.items():
				if name != to_del:
					modified_inv.append(item)
			self.inventory = modified_inv

	def pick_item(self):
		'''Chooses an item from inventory'''
		return self.inventory[0]

	def init_inv(self):
		'''Initialises inventory items from global'''
		inventory = []
		itemprobs = thing_itemprobs[self.name.lower()]
		for item, probs in itemprobs.items():
			choices = []
			choices.append((True,probs[0]))
			choices.append((False,probs[1]))
			to_add = weighted_choice(choices)
			if to_add:
				inventory.append(Item(item))
		return inventory

	def incur_damage(self, damage):
		'''Incurs damage on self, destroys if health<=0'''
		self.health -= damage
		new_health = self.health
		if new_health <= 0:
			self.destroy()

	def destroy(self):
		'''Flags self for destruction'''
		self.destroyed = True

	def init_prop(self):
		'''Puts property values from global dict'''
		return thing_properties[self.name.lower()]

	def init_desc(self):
		'''Chooses a description from global list'''
		return weighted_choice(thing_descs[self.name.lower()])

	def init_res(self):
		'''Generates resource dict from global dict'''
		raw_dict = thing_resources[self.name.lower()]
		res_dict = {}
		for resource, weights in raw_dict.items():
			res_dict[resource] = random.randint(weights[0], weights[1])
		return res_dict


class Space(Thing):
	def __init__(self, pos):
		Thing.__init__(self, pos)


class Asteroid(Thing):
	def __init__(self, pos):
		Thing.__init__(self, pos)


class Nebula(Thing):
	def __init__(self, pos):
		Thing.__init__(self, pos)


class Station(Thing):
	def __init__(self, pos):
		Thing.__init__(self, pos)
		self.is_shop = self.init_shop()
		if self.is_shop:
			self.description = 'Trading station, buying and selling'

	def init_shop(self):
		'''Tells if station is a shop'''
		yes_shop = self.properties['shop'][0]
		not_shop = self.properties['shop'][1]
		choices = [(True,yes_shop),(False,not_shop)]
		return weighted_choice(choices)

class Planet(Thing):
	def __init__(self, pos):
		Thing.__init__(self, pos)


class Ship(Thing):
	def __init__(self, pos):
		Thing.__init__(self, pos)

class Debris(Thing):
	def __init__(self, pos):
		Thing.__init__(self, pos)

class Categories(object):
	Habitable = 1
	Nebula = 2
	MineralRich = 3
	Populated = 4
	Typical = 5
	Shop = 6

