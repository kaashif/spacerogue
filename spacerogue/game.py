import random
import json
from spacerogue.thing import Asteroid, Station, Planet, Space, Categories
from spacerogue.player import Player
from spacerogue.universe import Universe
from spacerogue.mapping import Map
from spacerogue.gameutil import clear, valid_dir, titleize, cat
from spacerogue.screen import Screen
from spacerogue.shop import Shop
from pkg_resources import resource_filename
from curses import KEY_LEFT, KEY_DOWN, KEY_UP, KEY_RIGHT

class Game(object):
	def __init__(self, screen):
		self.running = False
		self.universe = None
		self.player = None
		self.output = ''
		self.screen = screen
		self.mode = None

	def shopscreen(self):
		'''Opens shop screen'''
		if not self.player.near_shop():
			self.output = 'Player is not near a shop\n'
			return None
		else:
			shop = Shop(self.screen, self.player)
			while shop.running:
				shop.update_res()
				shop.show()
				shop.proc_input()

	def start(self):
		'''Begins game'''
		self.output = self.help('initrogue')
		self.running = True
		self.screen.update('', 'Loading universe...')
		self.screen.draw_all(player=False)
		self.screen.refresh()
		self.universe = Universe()
		self.player = Player()
		self.player.place(self.universe)
		self.screen.clear()
		while self.running:
			self.screen.update_map(Map(self.player), self.player.map_scope)
			self.screen.update(self.format_status(), self.format_mesg())
			self.screen.draw_all()
			self.screen.draw_side()
			self.screen.refresh()
			try:
				self.handle_command(self.get_rogue_cmd())
			except IndexError:
				self.output = 'Invalid command syntax'
			except (EOFError,KeyboardInterrupt):
				self.output = 'Please use "quit" or "exit" to close the game'

	def format_mesg(self):
		'''Formats messages, ready for display'''
		o = titleize('Messages')
		if len(self.output)>0 and self.output[-1] != '\n':
			o += "%s\n" % self.output
		else:
			o += str(self.output)
		if self.mode != 'rogue':
			o += titleize('Command Prompt')
		return o

	def format_status(self):
		'''Formats status, ready for display'''
		return self.player.show_status()

	def proc_key(self, key):
		'''Returns command associated with key'''
		cmd_dict = json.loads(cat('res/json/controls.json'))
		try:
			char = chr(key)
			return cmd_dict[char]
		except (KeyError,ValueError):
			raise LookupError

	def proc_mov(self, raw, speed='move'):
		'''Returns movement command associated with key'''
		i = ''
		if raw == ord('h') or raw == KEY_LEFT:
			i = '$$ left 1'
		elif raw == ord('j') or raw == KEY_DOWN:
			i = '$$ down 1'
		elif raw == ord('k') or raw == KEY_UP:
			i = '$$ up 1'
		elif raw == ord('l') or raw == KEY_RIGHT:
			i = '$$ right 1'
		return i.replace('$$', speed)

	def get_rogue_cmd(self):
		'''Gets single keypress, in rogue style'''
		raw = self.screen.get_key()
		try:
			i = self.proc_key(raw)
		except LookupError:
			if not self.player.warp_mode:
				i = self.proc_mov(raw, 'move')
			elif self.player.warp_mode:
				i = self.proc_mov(raw, 'warp')
		i = i.split(' ')
		return i

	def handle_command(self, cmd):
		'''Does something based on command'''
		current_system = self.universe.get_system(self.player.pos[0])
		current_sector = current_system.get_sector(self.player.pos[1])
		success = True
		if len(cmd) == 0:
			self.output = 'Please enter a command, or "help" for assistance'
		elif cmd[0] == 'help' and len(cmd) == 1:
			self.output = self.help('general')
		elif cmd[0] == 'help':
			self.output = self.help(cmd[1])
		elif cmd[0] == 'map':
			if cmd[1] == 'universe':
				self.player.map_scope = 'universe'
			elif cmd[1] == 'system':
				self.player.map_scope = 'system'
			elif cmd[1] == 'toggle':
				self.player.toggle_map()
		elif cmd[0] == 'scan':
			if cmd[1] == 'system':
				self.output = current_system.scan()
			elif cmd[1] == 'sector':
				self.output = current_sector.scan()
			elif cmd[1] == 'universe':
				self.output = 'Euclidean geometry with a hint of dark energy'
		elif cmd[0] == 'move':
			if valid_dir(cmd[1]):
				success = self.player.move_dir(cmd[1], cmd[2])
		elif cmd[0] == 'warp':
			if cmd[1] == 'toggle':
				self.output = self.player.warp_mode_toggle()
				self.player.toggle_map()
			elif valid_dir(cmd[1]):
				success = self.player.warp_dir(cmd[1], cmd[2])
		elif cmd[0] == 'mine':
			self.output = self.player.mine()
		elif cmd[0] == 'inv' or cmd[0] == 'inventory':
			self.output = self.player.show_inv()
		elif cmd[0] == 'abandon':
			self.running = False
		elif cmd[0] == 'attack':
			self.output = self.player.attack()
		elif cmd[0] == 'weapon':
			if cmd[1] == 'stats':
				self.output = self.player.weapon_stats()
			elif cmd[1] == 'cycle':
				self.output = self.player.weapon_cycle()
		elif cmd[0] == 'pickup':
			self.output = self.player.pick_up()
		elif cmd[0] == 'buysell':
			self.shopscreen()
		elif cmd[0] == 'exit' or cmd[0] == 'quit':
			import sys
			sys.exit(0)
		else:
			self.output = 'Invalid command, please try again.\n'
			success = False
		if success:
			self.universe.advance_time()

	def help(self, topic):
		'''Returns help string for specified topic'''
		try:
			return cat('res/help/%s.txt' % topic)
		except IOError:
			return 'Help for %s not found' % topic
