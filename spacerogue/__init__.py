from spacerogue.menu import Menu
from spacerogue.game import Game
from spacerogue.screen import Screen
from curses import curs_set

def start(stdscr):
	curs_set(0)
	screen = Screen(stdscr)
	main_menu = Menu(Game(screen), screen)
	while main_menu.running:
		main_menu.show()
		main_menu.handle_choice(main_menu.get_choice())
