from spacerogue.gameutil import cat
from spacerogue.props import weapon_props

class Item(object):
	'''Generic item, does nothing'''
	def __init__(self, name):
		self.name = name

class Weapon(Item):
	'''Equippable weapon'''
	def __init__(self, name):
		Item.__init__(self, name)
		self.properties = weapon_props[self.name]

	def show_stats(self, verbose=True):
		'''Displays weapon stats'''
		output = '%s\n' % self.name.title()
		for stat_name, value in self.properties.items():
			units = ''
			if stat_name == 'value':
				units = 'space dollar(s)'
			elif stat_name == 'damage':
				units = 'megajoules/shot'
			elif stat_name == 'ammo usage':
				units = 'kg/shot'
			output += '|- %s = %s %s\n' % (stat_name.title(), str(value), units)
		return output

