import os
import subprocess
import random
from pkg_resources import resource_filename

def time_between(t1, t2):
	'''Returns travel time, in days, between t1 and t2'''
	tfactor = 0.1
	if not t1.__class__.__name__ == t2.__class__.__name__:
		raise TypeError
	if t1.__class__.__name__ == 'StarSystem':
		tfactor = 10.0
		dist = ((t1.pos[0]-t2.pos[0])**2.0 + (t1.pos[1]-t2.pos[1])**2)**0.5
		return float(dist*tfactor)
	elif t1.__class__.__name__ == 'Sector':
		dist = ((t1.pos[1][0]-t2.pos[1][0])**2 + (t1.pos[1][1]-t2.pos[1][1])**2)**0.5
		return float(tfactor*dist)

def clear():
	if os.name == 'nt':
		subprocess.call(['cls'])
	else:
		subprocess.call(['clear'])


def gethelp(topic):
	'''Finds text file with help and returns its content'''
	filename = resource_filename(__name__, ('res/help/%s.txt' % topic))
	try:
		with open(filename, 'r') as helpfile:
			content = ''.join(helpfile.readlines())
			return content
	except IOError:
		return 'Help for %s not found' % topic


def valid_dir(d):
	'''Checks if it is a valid direction'''
	return (d == 'up' or d == 'down' or d == 'left' or d == 'right')


def weighted_choice(choices):
	'''Takes a list of choice/probability tuples, picks choice'''
	total = sum(w for c, w in choices)
	r = random.uniform(0, total)
	upto = 0
	for c, w in choices:
		if upto + w > r:
			return c
		upto += w

def cat(filename):
	'Returns file content'
	return ''.join(open(resource_filename(__name__, filename), 'r').readlines())

def titleize(string1, string2='', width=60):
	'''Changes string into titleized string'''
	title = '=- '
	title += string1.title()
	if string2 == '':
		title += ' -='
		title += '=' * (51-len(string1))
		title += '\n'
	else:
		title += ': '
		title += string2.title()
		title += ' -='
		title += '=' * (49-len(string1)-len(string2))
	return title
