import random
from spacerogue.thing import Ship, Space, Debris, Nebula
from spacerogue.items import Weapon, Item

class Player(Ship):
	def __init__(self):
		self.description = 'Your spaceship'
		self.pos = None
		self.map_scope = 'system'
		self.universe = None
		self.resources = {'rock': 0, 'metal': 0, 'dirt':
				0, 'gas': 0, 'gold': 0, 'hydrogen': 0, 'ammo':200}
		self.inv_capacity = 300
		self.inv_holding = 0
		self.inv_full = False
		self.weapons = [Weapon('railgun'), Weapon('laser'), Weapon('missile')]
		self.warp_mode = False
		self.destroyed = False
		self.current_weapon = 0
		self.health = 100
		self.inventory = [Item('artifact')]
		self.money = 0

	def show_money(self):
		'''Shows number of space dollar(s) player has'''
		return 'You have %s space dollar(s)' % self.money

	def near_shop(self):
		'''Tells if player is near trading station'''
		current_system = self.universe.get_system(self.pos[0])
		current_sector = current_system.get_sector(self.pos[1])
		thing = current_sector.pick_thing()
		return (thing.name.lower() == 'station' and thing.is_shop)

	def sell(self,res,amt):
		'''Sells amount of resource'''
		if (self.resources[res] - amt) < 0:
			return None
		else:
			self.resources[res] -= amt
			self.money += 10

	def buy(self,res,amt):
		'''Buys amount of resource'''
		if not self.inv_full:
			self.resources[res] += amt
			self.money -= 10

	def show_res(self):
		'''Shows resources, almost same as show_inv'''
		output = 'Your cargo hold contains:\n'
		for resource, amount in self.resources.items():
			output += '%s tons of %s\n' % (amount, resource)
		return output

	def add_item(self, name):
		'''Adds items with name to invnetory'''
		self.inventory.append(Item(name))

	def pick_up(self):
		'''Picks up items on thing in sector'''
		current_system = self.universe.get_system(self.pos[0])
		current_sector = current_system.get_sector(self.pos[1])
		thing = current_sector.pick_thing()
		to_pickup = thing.pick_item()
		thing.del_item(to_pickup.name)
		self.add_item(to_pickup.name)
		return 'Picked up %s' % to_pickup.name.lower()

	def reduce_ammo(self,amount):
		'''Uses amount ammo from ammo store'''
		a = self.resources['ammo']
		if (a-amount) <= 0:
			return False
		else:
			self.resources['ammo'] = (a-amount)
			return True

	def weapon_stats(self):
		'''Shows stats for each weapon'''
		output = ''
		for weapon in self.weapons:
			output += '%s\n' % weapon.show_stats()
		return output

	def warp_mode_toggle(self):
		'''Toggles warp drive'''
		self.warp_mode = not self.warp_mode
		if self.warp_mode:
			return 'Warp drive has been activated'
		else:
			return 'Warp drive has been deactivated'

	def weapon_cycle(self):
		'''Cycles current weapon'''
		num_wep = len(self.weapons)
		new_wep = self.current_weapon+1
		if new_wep >= num_wep:
			new_wep = 0
		self.current_weapon = new_wep
		return 'Switched to %s' % self.weapons[new_wep].name

	def attack(self):
		'''Attacks thing in sector, incurring damage'''
		current_system = self.universe.get_system(self.pos[0])
		current_sector = current_system.get_sector(self.pos[1])
		to_attack = current_sector.pick_thing()
		if to_attack.__class__ == Space:
			return 'You cannot attack empty space'
		elif to_attack.__class__ == Nebula:
			return 'You cannot attack a diffuse cloud of gas'
		cur_weapon = self.weapons[self.current_weapon]
		damage = cur_weapon.properties['damage']
		to_attack.incur_damage(damage)
		if not to_attack.destroyed:
			n = to_attack.name
			h = to_attack.health
			a = cur_weapon.properties['ammo usage']
			if self.reduce_ammo(a):
				output = 'Fired %s, using %s kg ammo\n' % (cur_weapon.name,a)
				output += 'Hit %s for %s hp damage\n' % (n.lower(), damage)
				output += '%s has %s hp remaining' % (n, h)
			else:
				return 'Not enough ammo!'
			return output
		elif to_attack.destroyed:
			return 'Destroyed %s' % to_attack.name.lower()

	def check_inv_stat(self):
		'''Returns inventory full/capacity string'''
		self.inv_holding = sum(self.resources.values())
		self.inv_full = (self.inv_holding >= self.inv_capacity)
		return 'Cargo hold: %s/%s tons\n' % (self.inv_holding, self.inv_capacity)

	def show_status(self):
		'''Returns status of ship'''
		output = self.check_inv_stat()
		if self.inv_full:
			output += 'CARGO HOLD IS FULL\n\n'
		output += 'You are in sector %s\n' % str(self.pos[1])
		output += '        in system %s\n\n' % str(self.pos[0])
		output += 'Hull is at %s hp\n\n' % self.health
		output += 'Ammo: %s kg\n\n' % self.resources['ammo'] 
		output += '%1.1f days since departure\n\n' % self.universe.time
		output += '%s space dollar(s)' % self.money
		return output

	def show_inv(self):
		'''Returns formatted string list of inventory stuff'''
		output = 'Your cargo hold contains:\n'
		for resource, amount in self.resources.items():
			output += '%s tons of %s\n' % (amount, resource)
		output += '\nItems:\n'
		for item in self.inventory:
			output += '%s\n' % item.name.title()
		return output

	def mine(self):
		'''Mines the objects in current sector'''
		if self.inv_full:
			return 'Cargo hold full, cannot mine'
		output = ''
		current_system = self.universe.get_system(self.pos[0])
		current_sector = current_system.get_sector(self.pos[1])
		to_mine = [
			t for t in current_sector.things if not isinstance(t, Player)]
		for thing in to_mine:
			for resource, amount in thing.resources.items():
				if amount != 0:
					if self.inv_holding+amount > self.inv_capacity:
						return 'Cannot mine, not enough cargo hold space'
					else:
						self.resources[resource] += amount
						thing.resources[resource] = 0
						output += 'Mined %s tons of %s from %s\n' % (amount,
																 resource, thing.name.lower())
		return output

	def warp(self, x, y):
		'''Warps based on string input'''
		current_system = self.universe.get_system(self.pos[0])
		uni_x = None
		uni_y = None
		try:
			uni_x = int(x)
			uni_y = int(y)
		except ValueError:
			return 'Invalid arguments'
		if 0 <= uni_x < self.universe.size and 0 <= uni_y < self.universe.size:
			new_system = self.universe.get_system((uni_x, uni_y))
			self.warp_to(current_system, new_system)
			return 'Warped to system %s' % str((uni_x, uni_y))
		else:
			return 'Invalid warp coordinates'

	def move(self, x, y):
		'''Moves to sector based on string input'''
		current_system = self.universe.get_system(self.pos[0])
		current_sector = current_system.get_sector(self.pos[1])
		sys_x = None
		sys_y = None
		try:
			sys_x = int(x)
			sys_y = int(y)
		except ValueError:
			return 'Invalid movement coordinates'
		if 0 <= sys_x < current_system.size and 0 <= sys_y < current_system.size:
			new_sector = current_system.get_sector((sys_x, sys_y))
			self.move_to(current_sector, new_sector)
			return 'Moved to sector %s' % str((sys_x, sys_y))
		else:
			return 'Coordinates are outside the system, use warp drive instead'

	def warp_dir(self, direction, str_amount):
		'''Moves player amount in dir, returns whether success'''
		amount = None
		try:
			amount = int(str_amount)
		except ValueError:
			return False
		current_system = self.universe.get_system(self.pos[0])
		new_system_pos = (None, None)
		if direction == 'up':
			new_system_pos = (self.pos[0][0], self.pos[0][1] - amount)
		elif direction == 'down':
			new_system_pos = (self.pos[0][0], self.pos[0][1] + amount)
		elif direction == 'left':
			new_system_pos = (self.pos[0][0] - amount, self.pos[0][1])
		elif direction == 'right':
			new_system_pos = (self.pos[0][0] + amount, self.pos[0][1])
		else:
			return 'Invalid direction, please try again'
		if self.universe.valid_pos(new_system_pos):
			new_system = self.universe.get_system(new_system_pos)
			self.warp_to(current_system, new_system)
		else:
			return False
		return True

	def move_dir(self, direction, str_amount):
		'''Moves player amount in dir, returns whether success'''
		amount = None
		try:
			amount = int(str_amount)
		except ValueError:
			return False
		current_system = self.universe.get_system(self.pos[0])
		current_sector = current_system.get_sector(self.pos[1])
		new_sector_pos = (None, None)
		if direction == 'up':
			new_sector_pos = (self.pos[1][0], self.pos[1][1] - amount)
		elif direction == 'down':
			new_sector_pos = (self.pos[1][0], self.pos[1][1] + amount)
		elif direction == 'left':
			new_sector_pos = (self.pos[1][0] - amount, self.pos[1][1])
		elif direction == 'right':
			new_sector_pos = (self.pos[1][0] + amount, self.pos[1][1])
		else:
			return 'Invalid direction, please try again'
		if current_system.valid_pos(new_sector_pos):
			new_sector = current_system.get_sector(new_sector_pos)
			self.move_to(current_sector, new_sector)
		else:
			return False
		return True

	def move_to(self, old_sector, new_sector):
		'''Moves player to new sector'''
		old_sector.del_thing(Player)
		new_sector.add_thing(self)
		self.pos = new_sector.pos

	def warp_to(self, old_system, new_system):
		'''Warps player to random sector in new system'''
		old_sector = old_system.get_sector(self.pos[1])
		old_sector.del_thing(Player)
		size = new_system.size
		get_rand = lambda x: random.randint(0, x - 1)
		universe_pos = new_system.pos
		system_pos = (get_rand(size), get_rand(size))
		new_pos = (universe_pos, system_pos)
		new_sector = new_system.get_sector(new_pos[1])
		new_sector.add_thing(self)
		self.pos = new_pos

	def place(self, universe):
		'''Picks random sector in random system and places player there'''
		self.universe = universe
		get_rand = lambda x: random.randint(0, x - 1)
		universe_x = get_rand(universe.size)
		universe_y = get_rand(universe.size)
		system_size = universe.get_system((0, 0)).size
		system_x = get_rand(system_size)
		system_y = get_rand(system_size)
		sector = universe.get_system(
			(universe_x, universe_y)).get_sector((system_x, system_y))
		self.pos = ((universe_x, universe_y), (system_x, system_y))
		sector.add_thing(self)

	def toggle_map(self):
		'''Toggles map scope'''
		if self.map_scope == 'system':
			self.map_scope = 'universe'
		else:
			self.map_scope = 'system'

