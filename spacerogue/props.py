import json
from spacerogue.gameutil import cat

system_all = json.loads(cat('res/json/system.json'))
system_descs = system_all['descriptions']
system_probs = system_all['probabilities']
thing_descs = {}
thing_resources = {}
thing_properties = {}
thing_itemprobs = {}
weapon_props = json.loads(cat('res/json/weapon.json'))

all_things = ['space', 'asteroid', 'station', 'planet', 'nebula', 'debris']
for thing in all_things:
	name = thing.lower()
	full_dict = json.loads(cat('res/json/%s.json' % name))
	desc_dict = full_dict['descriptions']
	res_dict = full_dict['resources']
	props_dict = full_dict['properties']
	itemprobs_dict = full_dict['items']
	descriptions = []
	resources = {}
	properties = {}
	itemprobs = {}
	for desc, weight in desc_dict.items():
		descriptions.append((desc, weight))
	thing_descs[name] = descriptions
	for resource, weights in res_dict.items():
		resources[resource] = weights
	thing_resources[name] = resources
	for prop, value in props_dict.items():
		properties[prop] = value
	thing_properties[name] = properties
	for item, itemprob in itemprobs_dict.items():
		itemprobs[item] = itemprob
	thing_itemprobs[thing] = itemprobs

