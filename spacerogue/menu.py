from spacerogue.gameutil import clear, cat
from spacerogue.screen import Screen
from pkg_resources import resource_filename


class Menu(object):
	def __init__(self, game, screen):
		self.game = game
		self.running = True
		self.output = ''
		self.screen = screen

	def get_choice(self):
		'''Prompts for choice'''
		return self.screen.get_key()

	def show(self):
		'''Displays the menu using the screen'''
		self.screen.maptext = ''
		self.screen.update('', '%s%s\n' % (cat('res/main.txt'), self.output))
		self.screen.draw_all(player=False)
		self.screen.refresh()

	def handle_choice(self, choice):
		'''Takes a curses keycode, takes action'''
		self.output = ''
		if choice == ord('1'):
			self.game.start()
		elif choice == ord('2'):
			self.output = self.game.help('menuhelp')
		elif choice == ord('3'):
			self.running = False
		else:
			self.output = '\nInvalid choice.'
