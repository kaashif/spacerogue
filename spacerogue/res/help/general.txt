Helpful commands:
    
    help [command] - display help for a command
    help list - list of help topics
    scan [target] - display info about target
    move [x] [y] - move to sector (x,y)
    move [direction] [n] - move [n] sectors in [direction]
    warp [x] [y] - warp to a new system
    railgun - fires railgun
    map universe - change to map of universe
    map system - change to map of current system
    mine - mine things in your sector
    inv - show contents of cargo hold
    abandon - abandons session, goes to main menu
    exit - closes program
