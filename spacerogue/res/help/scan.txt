Usage:

    scan [target]

Valid targets:

    sector, system, universe

What it does:

    Shows a list of descriptions of things in your current
    sector and the resources they have, which you can mine.

Example:

    scan sector

See also:

    mine
